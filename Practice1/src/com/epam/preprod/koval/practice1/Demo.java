package com.epam.preprod.koval.practice1;

import com.epam.preprod.koval.practice1.part1.Part1;
import com.epam.preprod.koval.practice1.part2.Part2;
import com.epam.preprod.koval.practice1part3.Part3;
import com.epam.preprod.koval.practice1part4.Part4;
import com.epam.preprod.koval.practice1part5.Part5;

/**
 * Demonstration Practice1.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
abstract class Demo {

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        Part1.main(new String[] {});
        Part2.main(new String[] { "75", "153" });
        Part3.main(new String[] { "75", "15" });
        Part4.main(new String[] { "12345" });
        Part5.main(new String[] { "AB", "180", "AA" });
    }
}
