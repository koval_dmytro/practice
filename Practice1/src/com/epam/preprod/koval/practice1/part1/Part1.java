package com.epam.preprod.koval.practice1.part1;

/**
 * Part1.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Part1 {
    /**
     * Hidden constructor.
     */
    private Part1() {

    }

    /**
     * Main function.
     * 
     * @param args args
     */
    public static void main(final String[] args) {
        if (args.length != 0) {
            System.out.println("Invalid arguments");
            return;
        }
        System.out.println("Hello, World!");
    }
}
