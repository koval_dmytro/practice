package com.epam.preprod.koval.practice1.part2;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Part2 {
    /**
     * Hidden constructor.
     */
    private Part2() {

    }

    /**
     * Main function.
     * 
     * @param args args
     */
    public static void main(final String[] args) {
        if (args.length != 2) {
            System.out.println("Invalid arguments");
            return;
        }
        final int numb1 = Integer.parseInt(args[0]);
        final int numb2 = Integer.parseInt(args[1]);
        final int actual = solve(numb1, numb2);
        final int expected = 10;
        System.out.println("Part2.Entered: " + numb1 + ", " + numb2
                + "; expected: " + expected + "; actual: " + actual);
    }

    /**
     * Summing two numbers.
     * 
     * @param first first
     * @param second second
     * @return sum
     */
    private static int solve(final int first, final int second) {
        int sum = first + second;
        return sum;
    }
}
