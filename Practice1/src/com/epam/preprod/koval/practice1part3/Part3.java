package com.epam.preprod.koval.practice1part3;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Part3 {
    /**
     * Hidden constructor.
     */
    private Part3() {

    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from comand line
     */
    public static void main(final String[] args) {
        if (args.length != 2) {
            System.out.println("Invalid arguments");
            return;
        }
        final int numb1 = Integer.parseInt(args[0]);
        final int numb2 = Integer.parseInt(args[1]);
        final int actual = solve(numb1, numb2);
        final int expected = 35;
        System.out.println("Part3.Entered: " + numb1 + ", " + numb2
                + "; expected: " + expected + "; actual: " + actual);
    }

    /**
     * Finding the greatest common divisor.
     * 
     * @param first first number
     * @param second second number
     * @return greatest common divisor
     */
    private static int solve(final Integer first, final Integer second) {
        Integer f = first;
        Integer s = second;
        int div = s;
        while (s != 0) {
            div = f % s;
            f = s;
            s = div;
        }
        return first;
    }
}
