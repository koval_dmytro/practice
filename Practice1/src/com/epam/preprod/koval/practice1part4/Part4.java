package com.epam.preprod.koval.practice1part4;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Part4 {

    /**
     * Hidden constructor.
     */
    private Part4() {

    }

    /**
     * Main function.
     * 
     * @param args args
     */
    public static void main(final String[] args) {
        final int numb = 10;
        if (args.length != 1) {
            System.out.println("Invalid arguments");
            return;
        }
        int number = Integer.parseInt(args[0]);
        int summ = 0;
        while (number != 0) {
            summ += number % numb;
            number /= numb;
        }
        System.out.println("Sum: " + summ);
    }
}
