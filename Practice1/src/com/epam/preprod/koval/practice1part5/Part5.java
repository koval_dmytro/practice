package com.epam.preprod.koval.practice1part5;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Part5 {

    /** ASCII. */
    private static final int ASCII = 64;

    /** Const. */
    private static final int CONST = 26;

    /**
     * Hidden constructor.
     */
    private Part5() {

    }

    /**
     * @param number number
     * @return int
     */
    private static int charsTodigits(final String number) {
        int digit = 0;
        int pow = number.length() - 1;

        for (int i = number.length(); i > 0; i--) {
            digit += (int) (number.charAt(number.length() - i) - ASCII)
                    * Math.pow(CONST, pow--);
        }
        return digit;
    }

    /**
     * @param number number
     * @return string
     */
    private static String digitsTochars(final int number) {
        StringBuilder sb = new StringBuilder();
        int modul;
        int temp = number;
        while (temp != 0) {
            modul = temp % CONST;
            if (modul == 0) {
                sb.append("Z");
                temp = (temp - 1) / CONST;
            } else {
                sb.append((char) (modul + ASCII));
                temp = (temp - modul) / CONST;
            }
        }

        return new String(sb);
    }

    /**
     * @param colum colum
     * @return string
     */
    private static String rightColumn(final String colum) {
        String chars = "";
        int num;
        num = charsTodigits(colum);
        num++;
        chars = digitsTochars(num);
        return chars;
    }

    /**
     * Main function.
     * 
     * @param args args
     */
    public static void main(final String[] args) {
        final int argsLength = 3;
        if (args.length != argsLength) {
            System.out.println("Invalid arguments");
            return;
        }
        String number = args[0];
        int digit = Integer.parseInt(args[1]);
        String colum = args[2];
        System.out.println(args[0] + " ===> " + charsTodigits(number));
        System.out.println(args[1] + " ===> " + digitsTochars(digit));
        System.out.println(args[2] + " ===> " + rightColumn(colum));
    }
}
