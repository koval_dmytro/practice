package com.epam.koval1.practice2;

import java.util.Iterator;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Demo {
    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        MyListImpl con = new MyListImpl();
        final int numb1 = 111;
        final int numb2 = 222;
        final int numb3 = 5555;
        con.add("A");
        con.add("B");
        con.add(numb1);
        con.add(numb2);

        System.out.println(con);

        con.remove(numb2);

        System.out.println(con);

        System.out.println("Size: " + con.size());

        System.out.println(con.contains(numb1));

        MyListImpl con1;
        con1 = con;
        System.out.println(con1.containsAll(con));

        Iterator<Object> it = con.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        ListIterator it1 = con.listIterator();
        while (it1.hasNext()) {
            System.out.println(it1.next());
        }

        it1.set(numb3);
        System.out.println(con);

        it1.remove();

        System.out.println(con);
    }
}
