package com.epam.koval1.practice2;

/**
 * @author Koval Dmitry
 * @version 1.0
 */
interface ListIterable {
    /**
     * Iterator.
     * 
     * @return list of iterator
     */
    ListIterator listIterator();
}
