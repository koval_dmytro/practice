package com.epam.koval1.practice2;

import java.util.Iterator;

/**
 * @author Koval Dmitry
 * @version 1.0
 */
public interface ListIterator extends Iterator<Object> {

    /**
     * Checks to see if the previous element for sampling.
     * 
     * @return true - have, false - not have
     */
    boolean hasPrevious();

    /**
     * Returns previous object.
     * 
     * @return object
     */
    Object previous();

    /**
     * Set object.
     * 
     * @param e object
     */
    void set(Object e);

    /**
     * Remove object.
     */
    void remove();
}
