package com.epam.koval1.practice2;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public interface MyList extends Iterable<Object> {
    /**
     * Add object.
     * 
     * @param e object
     */
    void add(Object e);

    /**
     * Clear of list.
     */
    void clear();

    /**
     * Remove object.
     * 
     * @param o object
     * @return true - successful, false - unsuccessful
     */
    boolean remove(Object o);

    /**
     * Returns to array.
     * 
     * @return array of object
     */
    Object[] toArray();

    /**
     * Size of list.
     * 
     * @return size
     */
    int size();

    /**
     * Contained in the object list.
     * 
     * @param o object
     * @return true - successful, false - unsuccessful
     */
    boolean contains(Object o);

    /**
     * Contained list in this list.
     * 
     * @param c list
     * @return true - successful, false - unsuccessful
     */
    boolean containsAll(MyList c);

}
