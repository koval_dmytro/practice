package com.epam.koval1.practice2;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public class MyListImpl implements MyList, ListIterable {

    /** Array. */
    private Object[] array = null;

    /** Size. */
    private static final int SIZE = 10;

    /** Counter. */
    private int count = 0;

    /**
     * Constructor.
     */
    public MyListImpl() {
        this.array = new Object[MyListImpl.SIZE];
    }

    @Override
    public final void add(final Object e) {
        if (array.length - count <= array.length / 2) {
            array = Arrays.copyOf(array, array.length * 2);
        }
        array[count++] = e;
    }

    @Override
    public final void clear() {
        this.array = null;
        count = 0;
    }

    @Override
    public final boolean remove(final Object o) {

        int index = -1;
        for (int i = 0; i < size(); i++) {
            if (o.equals(this.array[i])) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            int numMoved = count - index - 1;
            System.arraycopy(this.array, index + 1, this.array, index,
                    numMoved);
            this.array[--count] = null;
            return true;
        }
        return false;
    }

    @Override
    public final Object[] toArray() {
        Object[] arrayList = this.array;
        return arrayList;
    }

    @Override
    public final int size() {
        return count;
    }

    @Override
    public final boolean contains(final Object o) {

        for (int i = 0; i < size(); i++) {
            if (o.equals(this.array[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public final boolean containsAll(final MyList c) {

        if (c.size() != size()) {
            return false;
        }
        for (int i = 0; i < size(); i++) {
            if (!c.toArray()[i].equals(array[i])) {
                return false;
            }
        }
        return true;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MyList = {");
        for (int i = 0; i < size(); i++) {
            sb.append("[" + array[i] + "],");
        }
        sb.append("}");
        return new String(sb);
    }

    @Override
    public final ListIterator listIterator() {
        return new ListIteratorImpl();
    }

    @Override
    public final Iterator<Object> iterator() {
        return new IteratorImpl();
    }

    /**
     * Iterator implementation.
     * 
     * @author Koval Dmytro
     * @version 1.0
     */
    private class IteratorImpl implements Iterator<Object> {

        /** Index of next element to return. */
        private int cursor;

        /** Index of last element returned; -1 if no such. */
        private int lastRet = -1;

        /**
         * Gets the value of {@linkplain MyListImpl.IteratorImpl#cursor}.
         * 
         * @return value of {@linkplain MyListImpl.IteratorImpl#cursor}
         */
        public int getCursor() {
            return cursor;
        }

        /**
         * Sets {@linkplain MyListImpl.IteratorImpl#cursor} with new value.
         * 
         * @param aCursor new value for
         *            {@linkplain MyListImpl.IteratorImpl#cursor}
         */
        public void setCursor(final int aCursor) {
            this.cursor = aCursor;
        }

        /**
         * Gets the value of {@linkplain MyListImpl.IteratorImpl#lastRet}.
         * 
         * @return value of {@linkplain MyListImpl.IteratorImpl#lastRet}
         */
        public int getLastRet() {
            return lastRet;
        }

        /**
         * Sets {@linkplain MyListImpl.IteratorImpl#lastRet} with new value.
         * 
         * @param aLastRet new value for
         *            {@linkplain MyListImpl.IteratorImpl#lastRet}
         */
        public void setLastRet(final int aLastRet) {
            this.lastRet = aLastRet;
        }

        /**
         * Checks to see if the next element is next to the sampling method.
         * 
         * @return true - has, false - not
         */
        public boolean hasNext() {
            return cursor != count;
        }

        /**
         * Returns next element.
         * 
         * @return object
         */
        public Object next() {
            int i = cursor;
            if (i >= count) {
                throw new NoSuchElementException();
            }
            Object[] elementData = MyListImpl.this.toArray();
            if (i >= elementData.length) {
                throw new ConcurrentModificationException();
            }
            cursor = i + 1;
            lastRet = i;
            return (Object) elementData[lastRet];
        }

        /**
         * Remove object.
         */
        public void remove() {
            if (lastRet < 0) {
                throw new IllegalStateException();
            }
            try {
                MyListImpl.this.remove(MyListImpl.this.array[lastRet]);
                cursor = lastRet;
                lastRet = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }

    /**
     * List iterator implmentation.
     * 
     * @author Koval Dmytro
     * @version 1.0
     */
    private class ListIteratorImpl extends IteratorImpl
            implements ListIterator {

        @Override
        public boolean hasPrevious() {
            return getCursor() != 0;
        }

        @Override
        public Object previous() {
            int i = getCursor() - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            }
            Object[] elementData = MyListImpl.this.array;
            if (i >= elementData.length) {
                throw new ConcurrentModificationException();
            }
            setCursor(i);
            setLastRet(i);
            return (Object) elementData[getLastRet()];
        }

        @Override
        public void set(final Object e) {
            if (getLastRet() < 0) {
                throw new IllegalStateException();
            }
            try {
                array[getLastRet()] = e;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
