package com.epam.koval1.practice3;

import com.epam.koval1.practice3.part1.Demo1;
import com.epam.koval1.practice3.part2.Demo2;
import com.epam.koval1.practice3.part3.Demo3;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Demo {

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        System.out.println("=========================== PART1");
        Demo1.main(args);

        System.out.println("=========================== PART2");
        Demo2.main(args);

        System.out.println("=========================== PART3");
        Demo3.main(args);
    }
}
