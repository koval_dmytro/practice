package com.epam.koval1.practice3.part1;

import com.epam.koval1.practice3.part1.figure.Circle;
import com.epam.koval1.practice3.part1.figure.Figure;
import com.epam.koval1.practice3.part1.figure.Rectangle;
import com.epam.koval1.practice3.part1.figure.Square;

/**
 * Demonstrates Task1 functionality.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Demo1 {
    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        Figure[] figure = {
                new Square(Integer.parseInt("1"), Integer.parseInt("2"),
                        Integer.parseInt("3")),
                new Circle(Integer.parseInt("2"), Integer.parseInt("3"),
                        Integer.parseInt("4")),
                new Rectangle(Integer.parseInt("1"), Integer.parseInt("2"),
                        Integer.parseInt("3"), Integer.parseInt("4")) };
        Desc desc = new Desc(figure);

        desc.printNames();

        desc.add(new Square(Integer.parseInt("3"), Integer.parseInt("2"),
                Integer.parseInt("1")));
        System.out.println("~~~~~~~");
        desc.printNames();
        System.out.println("~~~~~~");
        desc.remove(1);
        desc.printNames();
        System.out.println("~~~~~~~");

        Figure[] figure1 = {
                new Circle(Integer.parseInt("22"), Integer.parseInt("33"),
                        Integer.parseInt("44")),
                new Rectangle(Integer.parseInt("11"), Integer.parseInt("22"),
                        Integer.parseInt("33"), Integer.parseInt("44")),
                new Square(Integer.parseInt("11"), Integer.parseInt("22"),
                        Integer.parseInt("33")),
                new Rectangle(Integer.parseInt("1"), Integer.parseInt("2"),
                        Integer.parseInt("2"), Integer.parseInt("3")) };
        Desc desc1 = new Desc(figure1);
        desc.add(desc1);
        desc.printNames();
    }
}
