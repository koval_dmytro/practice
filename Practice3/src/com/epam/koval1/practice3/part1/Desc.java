package com.epam.koval1.practice3.part1;

import java.util.Arrays;

import com.epam.koval1.practice3.part1.figure.Figure;

/**
 * List of figures.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Desc {
    /** Array of figures. */
    private Figure[] figures;

    /**
     * Constructor.
     * 
     * @param aFigures figure
     */
    public Desc(final Figure[] aFigures) {
        this.figures = aFigures;
    }

    /**
     * Area figure.
     * 
     * @return area
     */
    public final double area() {
        double area = 0;
        for (Figure fig : figures) {
            area += fig.area();
        }
        return area;
    }

    /**
     * Move figure.
     * 
     * @param dx dx
     * @param dy dy
     */
    public final void move(final double dx, final double dy) {
        for (Figure fig : figures) {
            fig.move(dx, dy);
        }
    }

    /**
     * Print name of figure.
     */
    public final void printNames() {
        for (Figure fig : figures) {
            System.out.println(fig.name());
        }
    }

    /**
     * Add figure.
     * 
     * @param f figure
     */
    public final void add(final Figure f) {
        figures = Arrays.copyOf(figures, figures.length + 1);
        figures[figures.length - 1] = f;
    }

    /**
     * Remove figure by index.
     * 
     * @param index index
     */
    public final void remove(final int index) {
        Figure[] figuresNew = new Figure[figures.length - 1];
        int count = 0;
        for (int i = 0; i < figures.length; i++) {
            if (index != i) {
                figuresNew[count++] = figures[i];
            }
        }
        figures = Arrays.copyOf(figuresNew, figuresNew.length);
    }

    /**
     * Add array figure to this array.
     * 
     * @param d desc
     */
    public final void add(final Desc d) {
        Figure[] figureNew = new Figure[figures.length + d.figures.length];
        System.arraycopy(figures, 0, figureNew, 0, figures.length);
        System.arraycopy(d.figures, 0, figureNew, figures.length,
                d.figures.length);
        figures = figureNew;
    }
}
