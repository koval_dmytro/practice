package com.epam.koval1.practice3.part1.figure;

/**
 * Circle.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Circle extends Figure {

    /** Radius. */
    private double radius;

    /**
     * Constructor.
     * 
     * @param x x
     * @param y y
     * @param aRadius radius
     */
    public Circle(final double x, final double y, final double aRadius) {
        super(x, y);
        this.radius = aRadius;
    }

    @Override
    public final double area() {
        return Math.PI * (radius * radius);
    }
}
