package com.epam.koval1.practice3.part1.figure;

/**
 * Figure.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Figure {
    /** X coordinate. */
    private double x;

    /** Y coordinate. */
    private double y;

    /**
     * Area of figure.
     * 
     * @return area
     */
    public abstract double area();

    /**
     * Constructor.
     * 
     * @param aX x
     * @param aY y
     */
    public Figure(final double aX, final double aY) {
        this.x = aX;
        this.y = aY;
    }

    /**
     * Move of figure.
     * 
     * @param dx dx
     * @param dy dy
     */
    public final void move(final double dx, final double dy) {
        this.x += dx;
        this.y += dy;
    }

    /**
     * Name of figure.
     * 
     * @return name
     */
    public final String name() {
        return this.getClass().getSimpleName();
    }

    /**
     * Gets the value of {@linkplain Figure#x}.
     * 
     * @return value of {@linkplain Figure#x}
     */
    public final double getX() {
        return x;
    }

    /**
     * Sets {@linkplain Figure#x} with new value.
     * 
     * @param aX new value for {@linkplain Figure#x}
     */
    public final void setX(final double aX) {
        this.x = aX;
    }

    /**
     * Gets the value of {@linkplain Figure#y}.
     * 
     * @return value of {@linkplain Figure#y}
     */
    public final double getY() {
        return y;
    }

    /**
     * Sets {@linkplain Figure#y} with new value.
     * 
     * @param aY new value for {@linkplain Figure#y}
     */
    public final void setY(final double aY) {
        this.y = aY;
    }
}
