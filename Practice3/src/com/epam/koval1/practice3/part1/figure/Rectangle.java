package com.epam.koval1.practice3.part1.figure;

/**
 * Rectangle.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Rectangle extends Figure {

    /** Side of a. */
    private double sideA;

    /** Side of b. */
    private double sideB;

    /**
     * Constructor.
     * 
     * @param x x
     * @param y y
     * @param aSideA side
     * @param aSideB side
     */
    public Rectangle(final double x, final double y, final double aSideA,
            final double aSideB) {
        super(x, y);
        this.sideA = aSideA;
        this.sideB = aSideB;
    }

    @Override
    public final double area() {
        return (sideA * sideB);
    }
}
