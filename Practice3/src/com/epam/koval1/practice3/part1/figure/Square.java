package com.epam.koval1.practice3.part1.figure;

/**
 * Square.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Square extends Figure {

    /** Side. */
    private double side;

    /**
     * Constructor.
     * 
     * @param x x
     * @param y y
     * @param aSide side
     */
    public Square(final double x, final double y, final double aSide) {
        super(x, y);
        this.side = aSide;
    }

    @Override
    public final double area() {
        return this.side * this.side;
    }

    /**
     * Gets the value of {@linkplain Square#side}.
     * 
     * @return value of {@linkplain Square#side}
     */
    public final double getSide() {
        return side;
    }

    /**
     * Sets {@linkplain Square#side} with new value.
     * 
     * @param aSide new value for {@linkplain Square#side}
     */
    public final void setSide(final double aSide) {
        this.side = aSide;
    }
}
