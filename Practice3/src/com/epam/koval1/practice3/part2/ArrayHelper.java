package com.epam.koval1.practice3.part2;

/**
 * Array helper.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
abstract class ArrayHelper {
    /**
     * Iterator.
     * 
     * @param ar array of object
     * @return iterator
     */
    public static Iterator iterator(final Object[] ar) {
        return new IteratorImpl(ar);
    }
}
