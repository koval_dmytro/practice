package com.epam.koval1.practice3.part2;

/**
 * Demonstration Task2.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Demo2 {
    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        Object[] ar = { Integer.parseInt("1"), Integer.parseInt("2"),
                Integer.parseInt("3") };
        Iterator it = ArrayHelper.iterator(ar);
        while (it.hasNext()) {
            System.out.print(it.next());
        }
        System.out.println();
        it.moveToStart();
        while (it.hasNext()) {
            System.out.print(it.next());
        }
        System.out.println();
        while (it.hasPrevious()) {
            System.out.print(it.previous());
        }
        System.out.println();
        it.moveToEnd();
        while (it.hasPrevious()) {
            System.out.print(it.previous());
        }
        System.out.println();
    }
}
