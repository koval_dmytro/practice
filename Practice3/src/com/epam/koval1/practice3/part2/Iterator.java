package com.epam.koval1.practice3.part2;

/**
 * Interface Iterator.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public interface Iterator {
    /**
     * Moves the internal pointer to the iterator to the beginning of array.
     */
    void moveToStart();

    /**
     * Move the internal iterator pointer at the end of the array.
     */
    void moveToEnd();

    /**
     * Checks to see if the next element for sampling.
     * 
     * @return true - has, false - not
     */
    boolean hasNext();

    /**
     * Checks to see if the previous element for sampling.
     * 
     * @return true - has, false - not
     */
    boolean hasPrevious();

    /**
     * Returns next object.
     * 
     * @return object
     */
    Object next();

    /**
     * Returns previous object.
     * 
     * @return object
     */
    Object previous();
}