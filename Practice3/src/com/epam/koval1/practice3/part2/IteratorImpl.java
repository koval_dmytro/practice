package com.epam.koval1.practice3.part2;

import java.util.NoSuchElementException;

/**
 * @author Koval Dmitry
 * @version 1.0
 */
public class IteratorImpl implements Iterator {

    /** Array. */
    private Object[] array;

    /** Pointer. */
    private int point = 0;

    /**
     * Constructor.
     * 
     * @param ar object
     */
    public IteratorImpl(final Object[] ar) {
        this.array = ar;
    }

    @Override
    public final void moveToStart() {
        point = 0;
    }

    @Override
    public final void moveToEnd() {
        point = array.length;
    }

    @Override
    public final boolean hasNext() {
        return point < array.length;
    }

    @Override
    public final boolean hasPrevious() {
        return point > 0;
    }

    @Override
    public final Object next() {
        if (hasNext()) {
            return array[point++];
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public final Object previous() {
        if (hasPrevious()) {
            return array[--point];
        } else {
            throw new NoSuchElementException();
        }
    }

}
