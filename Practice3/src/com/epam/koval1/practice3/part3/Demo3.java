package com.epam.koval1.practice3.part3;

import java.util.Arrays;

/**
 * Demonstration Task3.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Demo3 {
    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        final int length1 = 10;
        final int length2 = 5;
        StackImpl st = new StackImpl();
        for (int i = 0; i < length1; i++) {
            st.push(i);
        }
        System.out.println(st.peek());
        System.out.println(Arrays.asList(st.toArray()));

        for (int j = 0; j < length2; j++) {
            st.pop();
        }
        System.out.println(Arrays.asList(st.toArray()));
    }
}
