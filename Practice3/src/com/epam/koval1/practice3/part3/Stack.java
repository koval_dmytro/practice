package com.epam.koval1.practice3.part3;

/**
 * Interface Stack.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public interface Stack {
    /**
     * Check whether the stack is empty.
     * 
     * @return true - empty, false - not
     */
    boolean empty();

    // возвращает последний добавленный элемент
    /**
     * Returns the last inserted element.
     * 
     * @return object
     */
    Object peek();

    /**
     * The same as peek, but the element removes.
     * 
     * @return object
     */
    Object pop();

    /**
     * Add object to stack.
     * 
     * @param o object
     * @return object
     */
    Object push(Object o);
}
