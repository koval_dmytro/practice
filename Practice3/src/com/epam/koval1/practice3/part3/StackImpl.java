package com.epam.koval1.practice3.part3;

import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * Stack implementation.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class StackImpl implements Stack {
    /** Counter. */
    private int count = 0;

    /** Array. */
    private Object[] array = new Object[count];

    /**
     * Returns array.
     * 
     * @return array
     */
    public final Object[] toArray() {
        return array;
    }

    @Override
    public final boolean empty() {
        return array.length == 0;
    }

    @Override
    public final Object peek() {
        int len = array.length;
        if (len == 0) {
            throw new EmptyStackException();
        }
        return array[len - 1];
    }

    @Override
    public final Object pop() {
        Object obj;
        obj = peek();
        Object[] objectNew = new Object[array.length - 1];
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if ((array.length - 1) != i) {
                objectNew[counter++] = array[i];
            }
        }
        array = Arrays.copyOf(objectNew, objectNew.length);
        return obj;
    }

    @Override
    public final Object push(final Object o) {
        array = Arrays.copyOf(array, array.length + 1);
        array[count++] = o;
        return o;
    }
}
