package com.epam.koval1.Practice4;

import com.epam.koval1.Practice4.correlation.CorrelationOperation;
import com.epam.koval1.Practice4.file.ReadFile;

/**
 * Demostration Practice4.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Demo {
    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        CorrelationOperation cor = new CorrelationOperation(
                new ReadFile().listStudent());
        cor.getCorellationList();
        cor.showRes();
    }
}
