package com.epam.koval1.Practice4.correlation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.epam.koval1.Practice4.entity.Student;
import com.epam.koval1.Practice4.entity.StudentBean;

/**
 * @author Koval Dmitry
 * @version 1.0
 */
public class CorrelationOperation {
    /** List of student. */
    private List<Student> listStudent = null;

    /** List of correlation. */
    private List<StudentBean> listCorrel = new ArrayList<>();

    /** List of result correlation. */
    private List<StudentBean> listRes = new ArrayList<>();

    /**
     * Constructor.
     * 
     * @param list list
     */
    public CorrelationOperation(final List<Student> list) {
        listStudent = list;
    }

    /**
     * Constructor.
     */
    public CorrelationOperation() {

    }

    /**
     * Gets correlation list.
     */
    public final void getCorellationList() {
        Double xySum;
        Double xSum;
        Double ySum;
        Double xyMul;
        Double numerator;
        Double xSumSquare;
        Double ySumSquare;
        Double xSumInSquare;
        Double ySumInSquare;
        Double denominator;
        Double correl;

        for (int i = 0; i < listStudent.size(); i++) {
            for (int j = i + 1; j < listStudent.size(); j++) {
                if (listStudent.get(i) != listStudent.get(j)) {

                    xySum = xySum(listStudent.get(i), listStudent.get(j));
                    xSum = sum(listStudent.get(i));
                    ySum = sum(listStudent.get(j));
                    xyMul = xSum * ySum;
                    numerator = listStudent.get(i).getResult().length * xySum
                            - xyMul;
                    xSumSquare = sumSquare(listStudent.get(i));
                    ySumSquare = sumSquare(listStudent.get(j));
                    xSumInSquare = xSum * xSum;
                    ySumInSquare = ySum * ySum;
                    denominator = Math.sqrt(
                            (listStudent.get(i).getResult().length * xSumSquare
                                    - xSumInSquare)
                                    * (listStudent.get(i).getResult().length
                                            * ySumSquare - ySumInSquare));
                    correl = new BigDecimal(numerator / denominator)
                            .setScale(2, RoundingMode.DOWN).doubleValue();

                    listCorrel.add(new StudentBean(listStudent.get(i).getName(),
                            listStudent.get(i).getSurname(),
                            listStudent.get(j).getName(),
                            listStudent.get(j).getSurname(), correl));
                }
            }
        }

        Collections.sort(listCorrel, new Comparator<StudentBean>() {
            public int compare(final StudentBean st1, final StudentBean st2) {
                return st2.getCorrel().compareTo(st1.getCorrel());
            }
        });
    }

    /**
     * Show result.
     */
    public final void showRes() {
        final int length = 5;
        for (int i = 0; i < length; i++) {
            listRes.add(listCorrel.get(i));
        }
        listRes.forEach(x -> System.out.println(x));
    }

    /**
     * Summing in square.
     * 
     * @param stud student
     * @return number
     */
    private Double sumSquare(final Student stud) {
        Double sumSquare = 0.0;
        for (int i = 0; i < stud.getResult().length; i++) {
            sumSquare += (stud.getResult()[i]) * (stud.getResult()[i]);
        }
        return sumSquare;
    }

    /**
     * Summing x and y.
     * 
     * @param stud1 student
     * @param stud2 student
     * @return number
     */
    private Double xySum(final Student stud1, final Student stud2) {
        Double xy = 0.0;
        for (int i = 0; i < stud1.getResult().length; i++) {
            xy += stud1.getResult()[i] * stud2.getResult()[i];
        }
        return xy;
    }

    /**
     * Summing.
     * 
     * @param stud student
     * @return number
     */
    private Double sum(final Student stud) {
        Double sum = 0.0;
        for (int i = 0; i < stud.getResult().length; i++) {
            sum += stud.getResult()[i];
        }
        return sum;
    }
}
