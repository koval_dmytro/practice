package com.epam.koval1.Practice4.entity;

import java.util.Arrays;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public class Student {
    /** Name student. */
    private String name;

    /** Surname student. */
    private String surname;

    /** Result of tests. */
    private int[] result;

    /**
     * Constructor.
     */
    public Student() {

    }

    /**
     * Constructor.
     * 
     * @param aName name
     * @param aSurname surname
     * @param aResult array of result
     */
    public Student(final String aName, final String aSurname,
            final int[] aResult) {
        this.name = aName;
        this.surname = aSurname;
        this.result = aResult;
    }

    /**
     * Gets the value of {@linkplain Student#name}.
     * 
     * @return value of {@linkplain Student#name}
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets {@linkplain Student#name} with new value.
     * 
     * @param aName new value for {@linkplain Student#name}
     */
    public final void setName(final String aName) {
        this.name = aName;
    }

    /**
     * Gets the value of {@linkplain Student#surname}.
     * 
     * @return value of {@linkplain Student#surname}
     */
    public final String getSurname() {
        return surname;
    }

    /**
     * Sets {@linkplain Student#surname} with new value.
     * 
     * @param aSurname new value for {@linkplain Student#surname}
     */
    public final void setSurname(final String aSurname) {
        this.surname = aSurname;
    }

    /**
     * Gets the value of {@linkplain Student#result}.
     * 
     * @return value of {@linkplain Student#result}
     */
    public final int[] getResult() {
        return result;
    }

    /**
     * Sets {@linkplain Student#result} with new value.
     * 
     * @param aResult new value for {@linkplain Student#result}
     */
    public final void setResult(final int[] aResult) {
        this.result = aResult;
    }

    @Override
    public final String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Student [name=");
        builder.append(name);
        builder.append(", surname=");
        builder.append(surname);
        builder.append(", result=");
        builder.append(Arrays.toString(result));
        builder.append("]");
        return builder.toString();
    }

}
