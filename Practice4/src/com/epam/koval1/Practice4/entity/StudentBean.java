package com.epam.koval1.Practice4.entity;

/**
 * Student bean.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class StudentBean {
    /** First name student. */
    private String firstName;

    /** First surname student. */
    private String firstSurname;

    /** Second name student. */
    private String secondName;

    /** Second surname student. */
    private String secondSurname;

    /** Correlation. */
    private Double correl;

    /**
     * Constructor.
     * 
     * @param aFirstName first name
     * @param aFirstSurname first surname
     * @param aSecondName second name
     * @param aSecondSurname second surname
     * @param aCorrel correlation
     */
    public StudentBean(final String aFirstName, final String aFirstSurname,
            final String aSecondName, final String aSecondSurname,
            final Double aCorrel) {
        super();
        this.firstName = aFirstName;
        this.firstSurname = aFirstSurname;
        this.secondName = aSecondName;
        this.secondSurname = aSecondSurname;
        this.correl = aCorrel;
    }

    /**
     * Gets the value of {@linkplain StudentBean#firstName}.
     * 
     * @return value of {@linkplain StudentBean#firstName}
     */
    public final String getFirstName() {
        return firstName;
    }

    /**
     * Sets {@linkplain StudentBean#firstName} with new value.
     * 
     * @param aFirstName new value for {@linkplain StudentBean#firstName}
     */
    public final void setFirstName(final String aFirstName) {
        this.firstName = aFirstName;
    }

    /**
     * Gets the value of {@linkplain StudentBean#firstSurname}.
     * 
     * @return value of {@linkplain StudentBean#firstSurname}
     */
    public final String getFirstSurname() {
        return firstSurname;
    }

    /**
     * Sets {@linkplain StudentBean#firstSurname} with new value.
     * 
     * @param aFirstSurname new value for {@linkplain StudentBean#firstSurname}
     */
    public final void setFirstSurname(final String aFirstSurname) {
        this.firstSurname = aFirstSurname;
    }

    /**
     * Gets the value of {@linkplain StudentBean#secondName}.
     * 
     * @return value of {@linkplain StudentBean#secondName}
     */
    public final String getSecondName() {
        return secondName;
    }

    /**
     * Sets {@linkplain StudentBean#secondName} with new value.
     * 
     * @param aSecondName new value for {@linkplain StudentBean#secondName}
     */
    public final void setSecondName(final String aSecondName) {
        this.secondName = aSecondName;
    }

    /**
     * Gets the value of {@linkplain StudentBean#secondSurname}.
     * 
     * @return value of {@linkplain StudentBean#secondSurname}
     */
    public final String getSecondSurname() {
        return secondSurname;
    }

    /**
     * Sets {@linkplain StudentBean#secondSurname} with new value.
     * 
     * @param aSecondSurname new value for
     *            {@linkplain StudentBean#secondSurname}
     */
    public final void setSecondSurname(final String aSecondSurname) {
        this.secondSurname = aSecondSurname;
    }

    /**
     * Gets the value of {@linkplain StudentBean#correl}.
     * 
     * @return value of {@linkplain StudentBean#correl}
     */
    public final Double getCorrel() {
        return correl;
    }

    /**
     * Sets {@linkplain StudentBean#correl} with new value.
     * 
     * @param aCorrel new value for {@linkplain StudentBean#correl}
     */
    public final void setCorrel(final Double aCorrel) {
        this.correl = aCorrel;
    }

    @Override
    public final String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(firstSurname);
        builder.append(";");
        builder.append(secondSurname);
        builder.append(";");
        builder.append(correl);
        return builder.toString();
    }
}
