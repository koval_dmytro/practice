package com.epam.koval1.Practice4.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.epam.koval1.Practice4.entity.Student;

/**
 * Read from file.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class ReadFile {
    /** Name of file. */
    private String nameFile = "src/com/epam/koval1/Practice4/input.txt";

    /** Array. */
    private ArrayList<String> array = new ArrayList<String>();

    /**
     * Read from file to list.
     * 
     * @return array
     */
    private ArrayList<String> readFile() {
        try {
            Scanner in = new Scanner(new File(nameFile));
            try {
                while (in.hasNext()) {
                    array.add(in.nextLine());
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return array;
    }

    /**
     * List of student.
     * 
     * @return list
     */
    public final ArrayList<Student> listStudent() {

        final String regex = "(?m)^(\\S*)\\s(\\w*)(\\w*)(.*\\w*)";

        List<String> line = readFile();
        List<Student> listStudent = new ArrayList<Student>();
        final int group = 4;
        for (String list : line) {
            Pattern pt = Pattern.compile(regex);
            Matcher matcher = pt.matcher(list);
            matcher.find();

            String listResult = matcher.group(group);
            String[] s = listResult.split(";");

            int[] arrays = new int[s.length - 1];
            for (int i = 0; i < arrays.length; i++) {
                arrays[i] = Integer.parseInt(s[i + 1]);
            }
            listStudent.add(
                    new Student(matcher.group(2), matcher.group(1), arrays));
        }
        return (ArrayList<Student>) listStudent;
    }
}
