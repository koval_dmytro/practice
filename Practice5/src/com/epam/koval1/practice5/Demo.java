package com.epam.koval1.practice5;

import com.epam.koval1.practice5.part1.FileHandler;
import com.epam.koval1.practice5.part2.Task2;
import com.epam.koval1.practice5.part3.UpperCase;
import com.epam.koval1.practice5.part4.Sha;
import com.epam.koval1.practice5.part5.Translate;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Demo {

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        System.out.println("=========================== PART1");
        FileHandler.main(args);

        System.out.println("=========================== PART2");
        Task2.main(args);

        System.out.println("=========================== PART3");
        UpperCase.main(args);

        System.out.println("=========================== PART4");
        Sha.main(args);

        System.out.println("=========================== PART5");
        Translate.main(args);
    }

}
