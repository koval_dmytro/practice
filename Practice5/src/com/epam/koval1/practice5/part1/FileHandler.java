package com.epam.koval1.practice5.part1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * File handler.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public final class FileHandler {
    /**
     * Constructor.
     */
    private FileHandler() {

    }

    /** Regular expression. */
    private static final String REGEX = "(\\w*).(\\w*).(\\w*).(\\w*.(\\S*))";

    /** Pattern. */
    private static Pattern pt;

    /** Matcher. */
    private static Matcher matcher;

    /**
     * First convert.
     * 
     * @param input text
     * @return text
     */
    public static String convert1(final String input) {
        final int gr = 4;
        pt = Pattern.compile(REGEX);
        matcher = pt.matcher(input);
        matcher.find();
        return (matcher.group(1) + " ==> " + matcher.group(gr));
    }

    /**
     * Second convert.
     * 
     * @param input text
     * @return text
     */
    public static String convert2(final String input) {
        final int grF = 3, grS = 2;
        pt = Pattern.compile(REGEX);
        matcher = pt.matcher(input);
        matcher.find();
        return (matcher.group(grS) + " (email: " + matcher.group(grF) + " )");
    }

    /**
     * Third convert.
     * 
     * @param input text
     * @return text
     */
    public static String convert3(final String input) {
        final int gr = 4;
        ReadFile r = new ReadFile();
        List<String> list = r.listString();
        pt = Pattern.compile("(\\w*).(\\w*\\s\\w*).(\\w*.(.*))");
        StringBuffer sb = new StringBuffer(input + " ==> ");
        for (String string : list) {
            matcher = pt.matcher(string);
            matcher.find();
            if (input.equals((matcher.group(gr)))) {
                sb.append(matcher.group(1) + ", ");
            }
        }
        return sb.toString();
    }

    /**
     * Fourth convert.
     * 
     * @param input text
     * @return text
     */
    public static String convert4(final String input) {
        Random r = new Random();
        final int min = 1000;
        final int max = 9000;
        pt = Pattern.compile(REGEX);
        matcher = pt.matcher(input);
        matcher.find();
        return (matcher.group() + ";" + String.valueOf(r.nextInt(max) + min));

    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        ReadFile r = new ReadFile();
        ArrayList<String> text = r.listString();
        for (String s : text) {
            System.out.println(FileHandler.convert1(s));
            System.out.println(FileHandler.convert2(s));
            System.out.println(FileHandler.convert4(s));
        }
        System.out.println(FileHandler.convert3("google.com"));
    }
}
