package com.epam.koval1.practice5.part1;

/**
 * Mail bean.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class MailBean {
    /** Domain. */
    private String domain;

    /** Mail. */
    private String mail;

    /** Login. */
    private String login;

    @Override
    public final String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MailBean [domen=");
        builder.append(domain);
        builder.append(", mail=");
        builder.append(mail);
        builder.append(", login=");
        builder.append(login);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Constructor.
     * 
     * @param aDomain domain
     * @param aMail mail
     * @param aLogin login
     */
    public MailBean(final String aDomain, final String aMail,
            final String aLogin) {
        super();
        this.domain = aDomain;
        this.mail = aMail;
        this.login = aLogin;
    }

    /**
     * Gets the value of {@linkplain MailBean#domain}.
     * 
     * @return value of {@linkplain MailBean#domain}
     */
    public final String getDomain() {
        return domain;
    }

    /**
     * Sets {@linkplain MailBean#domain} with new value.
     * 
     * @param aDomain new value for {@linkplain MailBean#domain}
     */
    public final void setDomain(final String aDomain) {
        this.domain = aDomain;
    }

    /**
     * Gets the value of {@linkplain MailBean#mail}.
     * 
     * @return value of {@linkplain MailBean#mail}
     */
    public final String getMail() {
        return mail;
    }

    /**
     * Sets {@linkplain MailBean#mail} with new value.
     * 
     * @param aMail new value for {@linkplain MailBean#mail}
     */
    public final void setMail(final String aMail) {
        this.mail = aMail;
    }

    /**
     * Gets the value of {@linkplain MailBean#login}.
     * 
     * @return value of {@linkplain MailBean#login}
     */
    public final String getLogin() {
        return login;
    }

    /**
     * Sets {@linkplain MailBean#login} with new value.
     * 
     * @param aLogin new value for {@linkplain MailBean#login}
     */
    public final void setLogin(final String aLogin) {
        this.login = aLogin;
    }
}
