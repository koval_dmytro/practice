package com.epam.koval1.practice5.part1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Read from File.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public final class ReadFile {
    /** Name of file. */
    private String nameFile = "src/com/epam/koval1/task1/input.txt";

    /** Array. */
    private ArrayList<String> array = new ArrayList<String>();

    /**
     * Read from file.
     * 
     * @return list of line
     */
    public ArrayList<String> readFile() {
        try {
            Scanner in = new Scanner(new File(nameFile));
            try {
                while (in.hasNext()) {
                    array.add(in.nextLine());
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return array;
    }

    /**
     * Read from text.
     * 
     * @return list of object
     */
    public ArrayList<String> listString() {

        final String regex = "(?s)(\\w*).(\\w*\\s\\w*).(.*)";

        List<String> line = readFile();
        List<String> listStudent = new ArrayList<String>();

        for (String list : line) {
            Pattern pt = Pattern.compile(regex);
            Matcher matcher = pt.matcher(list);
            matcher.find();
            listStudent.add(matcher.group(0));
        }
        return (ArrayList<String>) listStudent;
    }
}
