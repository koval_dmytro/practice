package com.epam.koval1.practice5.part2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Read from file.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class ReadFile {
    /** Name of file. */
    private String nameFile = "src/com/epam/koval1/task2/input.txt";

    /** Array. */
    private ArrayList<String> array = new ArrayList<String>();

    /**
     * Read from file.
     * 
     * @return list
     */
    public final ArrayList<String> readFile() {
        try {
            Scanner in = new Scanner(new File(nameFile));
            try {
                while (in.hasNext()) {
                    array.add(in.nextLine());
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return array;
    }
}
