package com.epam.koval1.practice5.part2;

/**
 * Demonstration Task2.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public abstract class Task2 {
    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        WordLength word = new WordLength();
        word.word();
    }
}
