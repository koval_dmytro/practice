package com.epam.koval1.practice5.part2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Find minimum and maximum word in text.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class WordLength {

    /** List of text. */
    private List<String> list = new ReadFile().readFile();

    /** List of word. */
    private List<String> listWord = new ArrayList<String>();

    /** Regular expression. */
    private static final String REGEX = "[A-z]+";

    /** Pattern. */
    private Pattern pt;

    /** Matcher. */
    private Matcher matcher;

    /**
     * Find minimum and maximum word and write to list.
     */
    public final void word() {
        pt = Pattern.compile(REGEX);

        for (String s : list) {
            matcher = pt.matcher(s);
            while (matcher.find()) {
                if (!listWord.contains(matcher.group())) {
                    listWord.add(matcher.group());
                }
            }
        }

        for (int i = listWord.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (listWord.get(j).length() > listWord.get(j + 1).length()) {
                    Collections.swap(listWord, j, j + 1);
                }
            }
        }
        final int length = 3;
        String[] min = new String[length];
        String[] max = new String[length];
        for (int i = 0; i < min.length; i++) {
            min[i] = listWord.get(i);
            max[i] = listWord.get(listWord.size() - length + i);
        }
        System.out.println("Min: " + min[0] + ", " + min[1] + ", " + min[2]);
        System.out.println("Max: " + max[0] + ", " + max[1] + ", " + max[2]);
    }
}
