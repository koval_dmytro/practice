package com.epam.koval1.practice5.part3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * File reader.
 * 
 * @author Koval Dmitry
 * @version 1.0
 */
public class FileRead {
    /** Name of file. */
    private String nameFile = "src/com/epam/koval1/task3/input.txt";

    /** Array. */
    private ArrayList<String> array = new ArrayList<String>();

    /**
     * Returns list of lines.
     * 
     * @return list
     */
    public final ArrayList<String> readFile() {
        try {
            Scanner in = new Scanner(new File(nameFile));
            try {
                while (in.hasNext()) {
                    array.add(in.nextLine());
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return array;
    }

}
