package com.epam.koval1.practice5.part3;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Replace word to upper case.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class UpperCase {
    /** List of lines. */
    private List<String> list = new FileRead().readFile();

    /** List of word. */
    private List<String> listWord = new ArrayList<>();

    /** Regular expression. */
    private static final String REGEX = "\\S+";

    /** Pattern. */
    private Pattern pt;

    /** Matcher. */
    private Matcher matcher;

    /**
     * Words to upper case.
     */
    private void upperLetter() {
        pt = Pattern.compile(REGEX);
        for (String s : list) {
            matcher = pt.matcher(s);
            while (matcher.find()) {
                char[] ch = matcher.group().toCharArray();
                ch[0] = Character.toUpperCase(ch[0]);
                listWord.add(new String(ch));
            }
        }
    }

    @Override
    public final String toString() {
        upperLetter();
        return listWord.toString();
    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        UpperCase up = new UpperCase();
        System.out.println(up.toString());
    }
}
