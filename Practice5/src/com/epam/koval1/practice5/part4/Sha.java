package com.epam.koval1.practice5.part4;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Secure Hash Algorithm.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Sha {
    /**
     * Constructor.
     */
    private Sha() {

    }

    /**
     * Encryption algorithm for text.
     * 
     * @param str text
     * @param hashName name of hash
     * @return hash text
     */
    public static String hash(final String str, final String hashName) {
        final int hex = 16;
        MessageDigest mDigest;
        StringBuffer sb = new StringBuffer();
        try {
            mDigest = MessageDigest.getInstance(hashName);
            byte[] byteData = mDigest.digest(str.getBytes());
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i]), hex).substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        System.out.println(hash("1", "MD5"));
    }
}
