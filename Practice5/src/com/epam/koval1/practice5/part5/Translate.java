package com.epam.koval1.practice5.part5;

/**
 * Translate number to different format.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Translate {
    /**
     * Constructor.
     */
    private Translate() {

    }

    /**
     * Base digit.
     * 
     * @param x number
     * @return number
     */
    private static String baseDigit(final int x) {
        StringBuilder sb = new StringBuilder();
        String s = Integer.toString(x);
        switch (s) {
        case "1":
            sb.append("I");
            return new String(sb);
        case "5":
            sb.append("V");
            return new String(sb);
        case "10":
            sb.append("X");
            return new String(sb);
        case "50":
            sb.append("L");
            return new String(sb);
        case "100":
            sb.append("C");
            return new String(sb);
        default:
            break;
        }
        return new String(sb);
    }

    /**
     * Translate decimal to roman fomat.
     * 
     * @param x1 number
     * @return roman
     */
    public static String decimalToRoman(final int x1) {
        final int max = 99;
        final int n1 = 4;
        final int n2 = 5;
        final int n3 = 9;
        final int n4 = 10;
        final int n5 = 99;

        int x = x1;
        if (x < 0 || x > max) {
            System.out.println("invalid number");
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int base = 0;
        while (x > 0) {
            if (x > 0 && x <= n3) {
                base = 1;
            } else if (x > n4 - 1 && x <= n5) {
                base = n4;
            }
            if (x >= n3 * base) {
                sb.append(baseDigit(base) + baseDigit(base * n4));
                x = x - n3 * base;
            } else if (x >= n2 * base) {
                sb.append(baseDigit(n2 * base));
                x = x - n2 * base;
            } else if (x >= n1 * base) {
                sb.append(baseDigit(base) + baseDigit(n2 * base));
                x = x - n1 * base;
            }
            while (x >= base) {
                sb.append(baseDigit(base));
                x = x - base;
            }
        }
        return new String(sb);
    }

    /**
     * Base digit.
     * 
     * @param x symbol
     * @return number
     */
    private static int baseDigit(final char x) {
        final int n1 = 1;
        final int n5 = 5;
        final int n10 = 10;
        final int n50 = 50;
        final int n100 = 100;
        switch (x) {
        case 'I':
            return n1;
        case 'V':
            return n5;
        case 'X':
            return n10;
        case 'L':
            return n50;
        case 'C':
            return n100;
        default:
            break;
        }
        return 0;
    }

    /**
     * Translate rome to decimal format.
     * 
     * @param x rome
     * @return digit
     */
    public static int romeToDecimal(final String x) {
        String digit = x;
        digit = x.toUpperCase();
        final int max = 100;
        int i = 0;
        int arab = 0;
        while (i < digit.length()) {
            char letter = digit.charAt(i);
            int number = baseDigit(letter);
            i++;
            if (i == digit.length()) {
                arab += number;
            } else {
                int nextNumber = baseDigit(digit.charAt(i));
                if (nextNumber > number) {
                    arab += (nextNumber - number);
                    i++;
                } else {
                    arab += number;
                }
            }
        }
        if (arab > max) {
            throw new NumberFormatException("invalid number");
        }
        return arab;
    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        System.out.println(decimalToRoman(Integer.parseInt("99")));
        System.out.println(romeToDecimal("LXXVI"));

    }
}
