package com.epam.koval1.Practice6;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.epam.koval1.Practice6.part1.Part1;
import com.epam.koval1.Practice6.part2.Part2;
import com.epam.koval1.Practice6.part3.Part3;
import com.epam.koval1.Practice6.part4.Part4;
import com.epam.koval1.Practice6.part5.Part5;

/**
 * Demo.
 * 
 * @author Koval Dmitry
 * @version 1.0
 */
public final class Demo {
    /**
     * Constructor.
     */
    private Demo() {

    }

    /** System in. */
    private static final InputStream STD_IN = System.in;

    /** Encoding. */
    private static final String ENCODING = "Cp1251";

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     * @throws IOException exception
     */
    public static void main(final String[] args) throws IOException {
        new Demo();
        System.out.println("=========================== PART1");
        Part1.main(args);

        System.out.println("=========================== PART2");
        Part2.main(args);

        System.out.println("=========================== PART3"); // set the mock
        // input
        System.setIn(new ByteArrayInputStream(
                "char\nString\nint\ndouble\n\nstop".getBytes(ENCODING)));
        Part3.main(args);
        System.setIn(STD_IN);

        System.out.println("=========================== PART4");
        Part4.main(args);

        System.out.println("=========================== PART5"); // set the
        System.setIn(new ByteArrayInputStream(
                "table ru\ntable en\napple ru\nasd\nasd ru\nstop"
                        .getBytes(ENCODING)));
        Part5.main(args); // restore the standard input
    }
}
