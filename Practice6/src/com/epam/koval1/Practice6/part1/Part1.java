package com.epam.koval1.Practice6.part1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Solves part1.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Part1 {
    /** Path to file. */
    private static final String FILE_NAME = "src/"
            + "com/epam/koval1/Practice6/part1/part1.txt";

    /** Encoding. */
    private static final String ENCODING = "UTF8";

    /** Regular expression. */
    private static final String REGEX = "(\\S+\\W)";

    /** Max word length. */
    private static final int MAX_WORD = 3;

    /**
     * Read from file.
     * 
     * @param aFileName name of file
     * @return string
     */
    public String getInput(final String aFileName) {
        StringBuilder sb = new StringBuilder();
        try {
            @SuppressWarnings("resource")
            Scanner scanner = new Scanner(new File(aFileName), ENCODING);
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine()).append(System.lineSeparator());
            }
            return sb.toString().trim();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Replace word to upper case.
     * 
     * @return string
     */
    public String replaceToUppercase() {
        String lines = getInput(FILE_NAME);
        Pattern pt = Pattern.compile(REGEX);
        Matcher matcher = pt.matcher(lines);
        StringBuilder sb = new StringBuilder();

        while (matcher.find()) {
            if (matcher.group().length() - 1 > MAX_WORD) {
                sb.append(matcher.group().toUpperCase());
            } else {
                sb.append(matcher.group());
            }
        }
        return new String(sb);
    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        Part1 p = new Part1();
        System.out.println(p.replaceToUppercase());
    }
}
