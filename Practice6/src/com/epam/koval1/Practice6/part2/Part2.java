package com.epam.koval1.Practice6.part2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

/**
 * Solves part2.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Part2 {
    /** Name of file unsorted. */
    private static final String FILE_NAME = "src"
            + "/com/epam/koval1/Practice6/part2/part2.txt";

    /** Name of file sorted. */
    private static final String FILE_NAME2 = "src"
            + "/com/epam/koval1/Practice6/part2/part2_sorted.txt";

    /** Array of input numbers. */
    private String[] arrayInput = new String[N];

    /** Array of output numbers. */
    private int[] arrayOutput = new int[N];

    /** Amount of numbers. */
    private static final int N = 10;

    /** maximum number of random. */
    private static final int MAX = 50;

    /**
     * Write input numbers of file.
     * 
     * @param aNameFile name of file
     * @throws IOException exception
     */
    public final void writeNumbers(final String aNameFile) throws IOException {
        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        for (int i = 0; i < N; i++) {
            if (i != N - 1) {
                sb.append(r.nextInt(MAX) + " ");
            } else {
                sb.append(r.nextInt(MAX));
            }
        }
        FileWriter writer = new FileWriter(aNameFile, false);
        writer.write(new String(sb));
        writer.flush();
        writer.close();

    }

    /**
     * Read from file.
     * 
     * @param aNameFile name of file
     * @return string
     */
    @SuppressWarnings("resource")
    public final String getInput(final String aNameFile) {
        StringBuilder sb = new StringBuilder();
        Scanner scanner;
        try {
            scanner = new Scanner(new File(aNameFile));
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine()).append(System.lineSeparator());
            }
            return sb.toString().trim();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sorting of array numbers.
     * 
     * @return array of int
     */
    public final int[] sortNumber() {
        String[] inputText = getInput(FILE_NAME).split(" ");
        int[] inputNumb = new int[inputText.length];
        for (int i = 0; i < inputText.length; i++) {
            inputNumb[i] = Integer.parseInt(inputText[i]);
        }
        arrayInput = inputText;
        for (int i = inputNumb.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (inputNumb[j] > inputNumb[j + 1]) {
                    int tmp = inputNumb[j];
                    inputNumb[j] = inputNumb[j + 1];
                    inputNumb[j + 1] = tmp;
                }
            }
        }
        return inputNumb;
    }

    /**
     * Show results.
     */
    public final void show() {
        StringBuilder sb = new StringBuilder();
        sb.append("input ==> ");
        for (int i = 0; i < arrayInput.length; i++) {
            sb.append(arrayInput[i] + " ");
        }
        sb.append("\noutput ==> ");
        for (int i = 0; i < arrayOutput.length; i++) {
            sb.append(arrayOutput[i] + " ");
        }
        System.out.println(sb);
    }

    /**
     * Write sort numbers in file.
     * 
     * @param aNameFile Name of file
     * @throws IOException exception
     */
    public final void writeSortNumbers(final String aNameFile)
            throws IOException {
        int[] array = sortNumber();
        arrayOutput = array;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i != array.length - 1) {
                sb.append(array[i] + " ");
            } else {
                sb.append(array[i]);
            }
        }
        FileWriter writer = new FileWriter(aNameFile, false);
        writer.write(new String(sb));
        writer.flush();
        writer.close();

    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     * @throws IOException exception
     */
    public static void main(final String[] args) throws IOException {
        Part2 p = new Part2();
        p.writeNumbers(FILE_NAME);
        p.writeSortNumbers(FILE_NAME2);
        p.show();
    }
}
