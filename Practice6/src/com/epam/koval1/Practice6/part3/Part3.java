package com.epam.koval1.Practice6.part3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Solves Part3.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Part3 {

    /** Name of file. */
    private static final String FILE_NAME = "src/"
            + "com/epam/koval1/Practice6/part3/part3.txt";

    /** Regular expression. */
    private static final String REG_SYMBOL = "[A-Za-zА-Яа-яЁё]+";

    /** Regular expression. */
    private static final String REG_NUMB = "(\\d?\\S?\\d+)+";

    /** Pattern. */
    private Pattern pt;

    /** Matcher. */
    private Matcher matcher;

    /**
     * Determines the type of data.
     * 
     * @param type data type
     * @return string
     */
    public final String getType(final String type) {
        if (type.length() == 0) {
            return "invalid type";
        }
        String s = getInput(FILE_NAME);
        StringBuilder sb = new StringBuilder();

        if (type.equals("char")) {
            sb.append("char ==> ");
            pt = Pattern.compile(REG_SYMBOL);
            matcher = pt.matcher(s);
            while (matcher.find()) {
                if (matcher.group().length() == 1) {
                    sb.append(matcher.group() + " ");
                }
            }
        } else if (type.equals("String")) {
            sb.append("String ==> ");
            pt = Pattern.compile(REG_SYMBOL);
            matcher = pt.matcher(s);
            while (matcher.find()) {
                if (matcher.group().length() > 1) {
                    sb.append(matcher.group() + " ");
                }
            }
        } else if (type.equals("int")) {
            sb.append("int ==> ");
            pt = Pattern.compile(REG_NUMB);
            matcher = pt.matcher(s);
            while (matcher.find()) {
                if (!matcher.group().contains(".")) {
                    sb.append(matcher.group() + " ");
                }
            }
        } else if (type.equals("double")) {
            sb.append("double ==> ");
            pt = Pattern.compile(REG_NUMB);
            matcher = pt.matcher(s);
            while (matcher.find()) {
                if (matcher.group().contains(".")) {
                    sb.append(matcher.group() + " ");
                }
            }
        } else {
            return "invalid type";
        }

        return new String(sb);
    }

    /**
     * Menu of program.
     */
    public final void menu() {
        @SuppressWarnings("resource")
        Scanner in = new Scanner(System.in);
        String type = null;
        for (;;) {
            type = in.nextLine();
            if (!type.equals("stop")) {
                System.out.println(getType(type));
            } else {
                break;
            }
        }
    }

    /**
     * Read from file.
     * 
     * @param aNameFile name of file
     * @return string
     */
    public final String getInput(final String aNameFile) {
        StringBuilder sb = new StringBuilder();
        try {
            @SuppressWarnings("resource")
            Scanner scanner = new Scanner(new File(aNameFile));
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine()).append(System.lineSeparator());
            }
            return sb.toString().trim();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        Part3 p = new Part3();
        p.menu();
    }
}
