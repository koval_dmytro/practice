package com.epam.koval1.Practice6.part4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser implementation Iterable.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Parser implements Iterable<String> {

    /** Regular expression. */
    private static String regex = "([^.?!]+\\S\\s?)";

    /** Pattern. */
    private Pattern pt = Pattern.compile(regex);

    /** Matcher. */
    private Matcher matcher = null;

    /** Text. */
    private String text;

    /**
     * Constructor.
     *
     * @param fileName file name
     * @param encoding encoding
     * @throws FileNotFoundException file not found
     */
    Parser(final String fileName, final String encoding)
            throws FileNotFoundException {
        text = read(fileName, encoding);
        matcher = pt.matcher(text);
    }

    /**
     * Read from file.
     * 
     * @param aFileName name of file
     * @param aEncoding encoding
     * @return line
     * @throws FileNotFoundException file not found
     */
    public final String read(final String aFileName, final String aEncoding)
            throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();

        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(new File(aFileName), aEncoding);
        while (scanner.hasNextLine()) {
            sb.append(scanner.nextLine()).append(System.lineSeparator());
        }
        return sb.toString().trim();

    }

    @Override
    public final Iterator<String> iterator() {
        return new IteratorParser();
    };

    /**
     * Iterator parser implementation.
     * 
     * @author Koval Dmytro
     * @version 1.0
     */
    private class IteratorParser implements Iterator<String> {

        @Override
        public boolean hasNext() {
            return matcher.find();
        }

        @Override
        public String next() {
            return matcher.group();
        }

        /**
         * Remove.
         */
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }
}
