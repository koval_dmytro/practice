package com.epam.koval1.Practice6.part4;

import java.io.FileNotFoundException;

/**
 * Demonstration Part4.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public final class Part4 {
    /**
     * Constructor.
     */
    private Part4() {

    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     * @throws FileNotFoundException file not found
     */
    public static void main(final String[] args) throws FileNotFoundException {
        new Part4();
        Parser p = new Parser("src/com/epam/koval1/Practice6/part4/part4.txt",
                "UTF8");
        for (String s : p) {
            System.out.println(s);
        }
    }
}
