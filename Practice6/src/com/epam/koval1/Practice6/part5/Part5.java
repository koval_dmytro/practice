package com.epam.koval1.Practice6.part5;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * Solves Part5.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class Part5 {

    /**
     * Translate word.
     */
    public final void translateKey() {
        @SuppressWarnings("resource")
        Scanner in = new Scanner(System.in);
        String type = null;
        for (;;) {
            type = in.nextLine();
            if (!type.equals("stop")) {
                String[] s = type.split(" ");
                try {
                    System.out.println(getKey(s[0], s[1]));
                } catch (MissingResourceException ex) {
                    System.out.println("invalid input");
                } catch (ArrayIndexOutOfBoundsException ex) {
                    System.out.println("invalid input");
                }
            } else {
                break;
            }
        }
    }

    /**
     * Get of word.
     * 
     * @param key key
     * @param local locale
     * @return string
     */
    public final String getKey(final String key, final String local) {
        Locale loc = new Locale(local);
        ResourceBundle bundle = ResourceBundle
                .getBundle("com.epam.koval1.Practice6.part5.resources", loc);
        return bundle.getString(key);
    }

    /**
     * Entry point for console program.
     * 
     * @param args incoming arguments from command line
     */
    public static void main(final String[] args) {
        Part5 p = new Part5();
        p.translateKey();
    }
}
