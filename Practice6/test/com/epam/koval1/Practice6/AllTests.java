package com.epam.koval1.Practice6;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import com.epam.koval1.Practice6.part1.Part1Test;
import com.epam.koval1.Practice6.part2.Part2Test;
import com.epam.koval1.Practice6.part3.Part3Test;
import com.epam.koval1.Practice6.part4.Part4Test;
import com.epam.koval1.Practice6.part5.Part5Test;

/**
 * @author Koval Dmytro
 * @version 1.0
 */
@RunWith(Suite.class)
@SuiteClasses({ DemoTest.class, Part1Test.class, Part2Test.class,
        Part3Test.class, Part4Test.class, Part5Test.class })
public class AllTests {

}
