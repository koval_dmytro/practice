package com.epam.koval1.Practice6;

import java.io.IOException;

import org.junit.Test;

/**
 * Demo test.
 * 
 * @author Koval Dmytro
 * @version 1.0
 */
public class DemoTest {

    @Test
    public void test() throws IOException {
        Demo.main(new String[0]);
    }

}
