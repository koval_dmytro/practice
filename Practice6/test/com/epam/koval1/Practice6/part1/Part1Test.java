package com.epam.koval1.Practice6.part1;

import static org.junit.Assert.*;
import org.junit.Test;

public class Part1Test {

    @Test
    public void testGetInput() {
        Part1 p = new Part1();
        String actual = p
                .getInput("src/com/epam/koval1/Practice6/part1/part1.txt");
        String expected = "Мама мы раму,Папа мыл машину.";
        assertEquals(expected, actual);
    }

    @Test
    public void testGetInput1() {
        Part1 p = new Part1();
        String actual = p.getInput("");
        String expected = null;
        assertEquals(expected, actual);
    }

    @Test
    public void testMain() {
        Part1.main(new String[0]);
    }

    @Test
    public void testReplaceToUppercase() {
        Part1 p = new Part1();
        String actual = p.replaceToUppercase();
        String expected = "МАМА мы РАМУ,ПАПА мыл МАШИНУ.";
        assertEquals(expected, actual);
    }
}
