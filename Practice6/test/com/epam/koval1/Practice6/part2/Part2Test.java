package com.epam.koval1.Practice6.part2;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

public class Part2Test {

    @Test
    public void testWriteNumbers() throws IOException {
        Part2 p = new Part2();
        p.writeNumbers("src/com/epam/koval1/Practice6/part2/part2.txt");
    }

    @Test(expected = FileNotFoundException.class)
    public void testWriteNumbers1() throws IOException {
        Part2 p = new Part2();
        p.writeNumbers("");
    }

    @Test
    public void testGetInput() {
        Part2 p = new Part2();
        String test = p
                .getInput("src/com/epam/koval1/Practice6/part2/part2.txt");
        String[] array = test.split(" ");
        assertEquals(10, array.length);
    }

    @Test
    public void testGetInput1() {
        Part2 p = new Part2();
        String actual = p.getInput("");
        String expected = null;
        assertEquals(expected, actual);
    }

    @Test
    public void testSortNumber() {
        Part2 p = new Part2();
        int[] actual = p.sortNumber();
        assertEquals(10, actual.length);
    }

    @Test
    public void testShow() {
        Part2 p = new Part2();
        p.show();
    }

    @Test
    public void testWriteSortNumbers() throws IOException {
        Part2 p = new Part2();
        p.writeSortNumbers(
                "src/com/epam/koval1/Practice6/part2/part2_sorted.txt");
    }

    @Test(expected = FileNotFoundException.class)
    public void testWriteSortNumbers1() throws IOException {
        Part2 p = new Part2();
        p.writeSortNumbers("");
    }

    @Test
    public void testMain() throws IOException {
        Part2.main(new String[0]);
    }

}
