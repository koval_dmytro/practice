package com.epam.koval1.Practice6.part3;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.Test;

public class Part3Test {

    @Test
    public void testMenu() {
        System.setIn(new ByteArrayInputStream(
                "char\nString\nint\ndouble\nasd\nstop".getBytes()));
        Part3 p = new Part3();
        p.menu();
        System.setIn(System.in);
    }

    @Test
    public void testGetInput() {
        Part3 p = new Part3();
        String actual = p
                .getInput("src/com/epam/koval1/Practice6/part3/part3.txt");
        assertEquals("a bcd 43.43 432 и л фвыа 89 .98", actual);
    }

    @Test
    public void testGetInput1() {
        Part3 p = new Part3();
        String actual = p.getInput("");
        String expected = null;
        assertEquals(expected, actual);
    }

    @Test
    public void testGetType() {
        Part3 p = new Part3();
        String actual = p.getType("char");
        assertEquals("char ==> a и л ", actual);
    }

    @Test
    public void testGetType0() {
        Part3 p = new Part3();
        String actual = p.getType("");
        assertEquals("invalid type", actual);
    }

    @Test
    public void testGetType1() {
        Part3 p = new Part3();
        String actual = p.getType("double");
        assertEquals("double ==> 43.43 .98 ", actual);
    }

    @Test
    public void testGetType2() {
        Part3 p = new Part3();
        String actual = p.getType("String");
        assertEquals("String ==> bcd фвыа ", actual);
    }

    @Test
    public void testGetType3() {
        Part3 p = new Part3();
        String actual = p.getType("int");
        assertEquals("int ==> 432 89 ", actual);
    }

    @Test
    public void testMain() {
        System.setIn(new ByteArrayInputStream(
                "char\nString\nint\ndouble\n\nstop".getBytes()));
        Part3.main(new String[0]);
        System.setIn(System.in);
    }
}
