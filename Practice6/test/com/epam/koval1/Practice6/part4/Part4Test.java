package com.epam.koval1.Practice6.part4;

import java.io.FileNotFoundException;
import java.util.Iterator;

import org.junit.Test;

public class Part4Test {

    @Test
    public void testMain() throws FileNotFoundException {
        Part4.main(new String[0]);
    }

    @Test(expected = FileNotFoundException.class)
    public void testReadFile() throws FileNotFoundException {
        new Parser("asd", "UTF8");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemove() throws FileNotFoundException {
        Parser p = new Parser("src/com/epam/koval1/Practice6/part4/part4.txt",
                "UTF8");
        Iterator it = p.iterator();
        it.remove();

    }

}
