package com.epam.koval1.Practice6.part5;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.Test;

public class Part5Test {

    @Test
    public void testGetKey() {
        Part5 p = new Part5();
        String actual = p.getKey("apple", "ru");
        assertEquals("яблоко", actual);
    }

    @Test
    public void testTranslateKey() {
        Part5 p = new Part5();
        System.setIn(new ByteArrayInputStream(
                "table ru\ntable en\napple ru\nasd ru\nasd ru\nasd\nstop"
                        .getBytes()));
        p.translateKey();
        System.setIn(System.in);
    }

    @Test
    public void testMain() {
        System.setIn(new ByteArrayInputStream(
                "table ru\ntable en\napple ru\nasd\nasd ru\nstop".getBytes()));
        Part5.main(new String[0]);
        System.setIn(System.in);
    }

}
